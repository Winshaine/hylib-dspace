<?xml version="1.0" encoding="UTF-8"?>

<!--
  remotesearch.xsl

  Extensible stylesheet for remote search function.

  This stylesheet transforms the remote search result table into a list 
  that looks exactly like the results from DSpace advanced search.

  Author: Kornschnok Dittawit

  Copyright (c) 2009, Knowledge Sharing - Collaborative Thinking Project,
  Asian Institute of Technology.  All rights reserved.

  Version: $Revision: 85 $
 
  Date: $Date: 2009-09-30 15:48:21 +0700 (Wed, 30 Sep 2009) $

-->

<xsl:stylesheet xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
	xmlns:dri="http://di.tamu.edu/DRI/1.0/"
	xmlns:mets="http://www.loc.gov/METS/"
	xmlns:xlink="http://www.w3.org/TR/xlink/"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:dim="http://www.dspace.org/xmlns/dspace/dim"
	xmlns:xhtml="http://www.w3.org/1999/xhtml"
	xmlns:mods="http://www.loc.gov/mods/v3"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="i18n dri mets xlink xsl dim xhtml mods dc">

	<xsl:template match="dri:table[contains(@id,'rmsearch-result-table')]">
		<ul class="ds-artifact-list">
			<xsl:apply-templates select="dri:row[contains(@id,'searchresult-row')]"/>
		</ul>      
    </xsl:template>

	<xsl:template match="dri:row[contains(@id,'searchresult-row')]">
	<!-- Uncomment this part and another part below if you want to make remote search format the date according to the browser's locale
		<xsl:param name="local-date">
    		<xsl:choose>
    			<xsl:when test="$locale='th'">
    				<xsl:value-of select="substring(dri:cell[4],1,string-length(dri:cell[4])-4)"/>
    				<xsl:value-of select="number(substring(dri:cell[4],string-length(dri:cell[4])-3,4))+543"/>
    			</xsl:when>
    			<xsl:otherwise>
    				<xsl:value-of select="dri:cell[4]"/>
    			</xsl:otherwise>    
    		</xsl:choose>	
    	</xsl:param>
	-->
		<li>
		   <div class="ds-artifact-item">
		   <xsl:call-template name="standardAttributes">
                <xsl:with-param name="class">ds-artifact-item 
                    <xsl:if test="(position() mod 2 = 0)">even</xsl:if>
                    <xsl:if test="(position() mod 2 = 1)">odd</xsl:if>
                </xsl:with-param>
           </xsl:call-template>
		   <div class="ds-artifact-item-with-popup">
			<div class="artifact-description">
				<div class="artifact-title">
					<a href="{dri:cell[1]/dri:xref/@target}"><xsl:value-of select="dri:cell[1]"/></a>
				</div>
				<div class="artifact-info">
					<xsl:choose>
						<xsl:when test="string-length(dri:cell[2])>0">
							<span class="author"><xsl:value-of select="dri:cell[2]"/></span>
						</xsl:when>
						<xsl:otherwise>
							<span class="author"><i18n:text>xmlui.dri2xhtml.METS-1.0.no-author</i18n:text></span>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:if test="string-length(dri:cell[4])>0">
						<span class="publisher-date"><xsl:text>(</xsl:text><span class="date"><xsl:value-of select="dri:cell[4]"/></span><xsl:text>)</xsl:text></span>
						<!--  <span class="publisher-date"><xsl:text>(</xsl:text><span class="date"><xsl:value-of select="$local-date"/></span><xsl:text>)</xsl:text></span> -->
					</xsl:if>
				</div>
			</div>
		   </div>
		  </div>
		</li>
	</xsl:template>
	
	<!-- The standard attributes template -->
    <xsl:template name="standardAttributes">
        <xsl:param name="class"/>
        <xsl:if test="@id">
            <xsl:attribute name="id"><xsl:value-of select="translate(@id,'.','_')"/></xsl:attribute>
        </xsl:if>
        <xsl:attribute name="class">
            <xsl:value-of select="normalize-space($class)"/>
            <xsl:if test="@rend">
                <xsl:text> </xsl:text>
                <xsl:value-of select="@rend"/>
            </xsl:if>
        </xsl:attribute>
    </xsl:template>
	
</xsl:stylesheet>
