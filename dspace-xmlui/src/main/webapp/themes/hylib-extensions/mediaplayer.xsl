<?xml version="1.0" encoding="UTF-8"?>

<!--
  mediaplayer.xsl

  Media Player extension.

  Use Flash-based JW FLV Media Player to play media file in item view.
  Currently supported .mp4, .flv, .mp4 and .aac.

  Author: Kornschnok Dittawit
  Author: Than Htut Soe 2013

  Copyright (c) 2013, PurnSarn.  All rights reserved.

  Version: $Revision: 88 $
 
  Date: $Date: 2009-10-08 14:47:39 +0700 (Thu, 08 Oct 2009) $

  Based on code from: http://wiki.dspace.org/index.php?title=DSpace_1.5_XMLUI_FLV_Video_Progressive_Download/

  JW FLV Media Player is only for non-commercial use only.

  JW FLV Media Player (http://www.longtailvideo.com/players/jw-flv-player/)
  is copyrighted by LongTail Ad Solutions and distributed under the
  Creative Commons Attribution-Noncommercial-Share Alike 3.0 license.
-->

<xsl:stylesheet
    xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
    xmlns:dri="http://di.tamu.edu/DRI/1.0/"
    xmlns:mets="http://www.loc.gov/METS/"
    xmlns:xlink="http://www.w3.org/TR/xlink/"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:dim="http://www.dspace.org/xmlns/dspace/dim"
    xmlns:xhtml="http://www.w3.org/1999/xhtml"
    xmlns:mods="http://www.loc.gov/mods/v3"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns="http://www.w3.org/1999/xhtml"
    exclude-result-prefixes="i18n dri mets xlink xsl dim xhtml mods dc">
    
    <xsl:output indent="yes"/>

    <!-- Generate video player objects for movie files in the CONTENT bundle -->
    <xsl:template name="showMediaPlayer">
        <xsl:for-each select="./mets:fileSec/mets:fileGrp[@USE='CONTENT']/mets:file">
            <xsl:call-template name="showMediaPlayerItem">
                <!-- Last 4 letters of URL is file extenstion 
                    (DSpace currently doesn't have matches or ends-with) -->
                <xsl:with-param name="fileext" select="substring(mets:FLocat[@LOCTYPE='URL']/@xlink:title, string-length(mets:FLocat[@LOCTYPE='URL']/@xlink:title)-3)"/>
            </xsl:call-template>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="showMediaPlayerItem">
        <xsl:param name="fileext"/>

        <xsl:if test="contains('.mp4 .flv', $fileext)">
            <xsl:call-template name="showVideoPlayer"/>
        </xsl:if>
        <xsl:if test="contains('.mp3 .aac', $fileext)">
            <xsl:call-template name="showAudioPlayer"/>
        </xsl:if>
    </xsl:template>

 <!--    <xsl:template name="showVideoPlayer">
        <div class="hylib-mediaplayer">
            <! Display the name of bitstream 
            <p><b><xsl:value-of select="mets:FLocat[@LOCTYPE='URL']/@xlink:title"/></b></p>

            <! Display video player 
            <p>
            <embed src="{$theme-path}/../hylib-extensions/mediaplayer/player.swf" 
                PLUGINSPAGE="http://www.macromedia.com/go/getflashplayer" 
                TYPE="application/x-shockwave-flash" 
                WIDTH="480" HEIGHT="385" 
                allowfullscreen="true" 
                flashvars="file={mets:FLocat/@xlink:href}&amp;type=video&amp;skin={$theme-path}/../hylib-extensions/mediaplayer/modieus.swf" />
            <br /><br />
            </p>
        </div>
    </xsl:template>        

    <xsl:template name="showAudioPlayer">
        <div class="hylib-mediaplayer">
            <! Display the name of bitstream 
            <p><b><xsl:value-of select="mets:FLocat[@LOCTYPE='URL']/@xlink:title"/></b></p>

            <! Display video player 
            <p>
            <embed src="{$theme-path}/../hylib-extensions/mediaplayer/player.swf" 
                PLUGINSPAGE="http://www.macromedia.com/go/getflashplayer" 
                TYPE="application/x-shockwave-flash" 
                WIDTH="480" HEIGHT="85" 
                allowfullscreen="true" 
                flashvars="file={mets:FLocat/@xlink:href}&amp;type=sound&amp;skin={$theme-path}/../hylib-extensions/mediaplayer/modieus.swf" />
            <br /><br />
            </p>
        </div>
    </xsl:template>        -->

	
    <xsl:template name="showVideoPlayer">
        <div class="hylib-mediaplayer">
            <!-- loading the player by JWPlayer 6.0 -->
			 
			<div id="myVideo">Loading the player ...</div>
			<script type="text/javascript" file="{mets:FLocat/@xlink:href}">
			jwplayer("myVideo").setup({
				file: "<xsl:value-of select="mets:FLocat/@xlink:href" />",
				type : "mp4",
				height: 385,
				width: 480,
				autostart: "false"
			});
			</script>
        </div>
    </xsl:template>        

    <xsl:template name="showAudioPlayer">
        <div class="hylib-mediaplayer">
            <!-- Display the name of bitstream -->
            <p><b><xsl:value-of select="mets:FLocat[@LOCTYPE='URL']/@xlink:title"/></b></p>

            <!-- Display audio player player -->
            <p>
			<div id="myAudio">Loading the player ...</div>
			<script type="text/javascript" file="{mets:FLocat/@xlink:href}">
			jwplayer("myAudio").setup({
				file: "<xsl:value-of select="mets:FLocat/@xlink:href" />",
				type : "mp3",
				height: 30,
				width: 480,
				autostart: "false"
			});
			</script>
            </p>
        </div>
    </xsl:template>   
	
</xsl:stylesheet>
