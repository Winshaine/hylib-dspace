<?xml version="1.0" encoding="UTF-8"?>

<!--
  dateformat.xsl

  Extensible stylesheet for formatting dates according to the locale.

  IMPORTANT! 
  This stylesheet must be used with HyLib Mirage or themes that are based on 
  HyLib Mirage. Otherwise, please make sure the theme you use receives locale 
  parameter from the transformer.

  Author: Kornschnok Dittawit, Pongtawat C.

  Copyright (c) 2009-2012, Knowledge Sharing - Collaborative Thinking Project,
  Asian Institute of Technology.  All rights reserved.

  Version: $Revision: 85 $
 
  Date: $Date: 2009-09-30 15:48:21 +0700 (Wed, 30 Sep 2009) $

-->

<xsl:stylesheet xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
    xmlns:dri="http://di.tamu.edu/DRI/1.0/"
    xmlns:mets="http://www.loc.gov/METS/"
    xmlns:xlink="http://www.w3.org/TR/xlink/"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:dim="http://www.dspace.org/xmlns/dspace/dim"
    xmlns:xhtml="http://www.w3.org/1999/xhtml"
    xmlns:mods="http://www.loc.gov/mods/v3"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:encoder="xalan://java.net.URLEncoder"
    xmlns="http://www.w3.org/1999/xhtml"
    exclude-result-prefixes="i18n dri mets xlink xsl dim xhtml mods dc encoder">

    <xsl:template name="formatDate">
        <xsl:param name="date"/>
        <xsl:choose>
            <xsl:when test="$locale='th'">
                <xsl:copy-of select="number(substring($date,1,4))+543"/>
                <xsl:copy-of select="substring($date,5,6)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:copy-of select="substring($date,1,10)"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
	
</xsl:stylesheet>
