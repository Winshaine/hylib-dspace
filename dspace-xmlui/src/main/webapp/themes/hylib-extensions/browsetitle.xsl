<?xml version="1.0" encoding="UTF-8"?>

<!--
  browsetitle.xsl

  Extensible stylesheet for browsing by titles with Thai characters

  Author: Kornschnok Dittawit

  Copyright (c) 2010, Knowledge Sharing - Collaborative Thinking Project,
  Asian Institute of Technology.  All rights reserved.

  Version: $Revision: 85 $
 
  Date: $Date: 2010-10-06 23:19:21 +0700 (Thu, 6 Jun 2010) $

-->

<xsl:stylesheet xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
	xmlns:dri="http://di.tamu.edu/DRI/1.0/"
	xmlns:mets="http://www.loc.gov/METS/"
	xmlns:xlink="http://www.w3.org/TR/xlink/"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:dim="http://www.dspace.org/xmlns/dspace/dim"
	xmlns:xhtml="http://www.w3.org/1999/xhtml"
	xmlns:mods="http://www.loc.gov/mods/v3"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:encoder="xalan://java.net.URLEncoder"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="i18n dri mets xlink xsl dim xhtml mods dc encoder">
    
    <xsl:template match="dri:list[contains(@id, 'aspect.artifactbrowser.ConfigurableBrowse.list.jump-list')]">
		<xsl:param name="base_url">
			<xsl:text>browse?rpp=</xsl:text>
			<xsl:value-of select="../dri:p[contains(@id, 'aspect.artifactbrowser.ConfigurableBrowse.p.hidden-fields')]/dri:field[contains(@n, 'rpp')]/dri:value/text()"/>
			<xsl:text>&amp;order=</xsl:text>
			<xsl:value-of select="../dri:p[contains(@id, 'aspect.artifactbrowser.ConfigurableBrowse.p.hidden-fields')]/dri:field[contains(@n, 'order')]/dri:value/text()"/>
			<xsl:text>&amp;sort_by=</xsl:text>
			<xsl:value-of select="../dri:p[contains(@id, 'aspect.artifactbrowser.ConfigurableBrowse.p.hidden-fields')]/dri:field[contains(@n, 'sort_by')]/dri:value/text()"/>
			<xsl:text>&amp;etal=</xsl:text>
			<xsl:value-of select="../dri:p[contains(@id, 'aspect.artifactbrowser.ConfigurableBrowse.p.hidden-fields')]/dri:field[contains(@n, 'etal')]/dri:value/text()"/>
			<xsl:text>&amp;type=</xsl:text>
			<xsl:value-of select="../dri:p[contains(@id, 'aspect.artifactbrowser.ConfigurableBrowse.p.hidden-fields')]/dri:field[contains(@n, 'type')]/dri:value/text()"/>
		</xsl:param>
		
		<xsl:apply-templates select="dri:head"/>
        <ul>
			<xsl:call-template name="standardAttributes">
                <xsl:with-param name="class">ds-simple-list</xsl:with-param>
            </xsl:call-template>
            <xsl:apply-templates select="*[not(name()='head')]" mode="nested"/>
		<div class="hylib-thai-browse">
			<li>
				<a class="">
					<xsl:attribute name="href"><xsl:value-of select="concat($base_url, '&amp;starts_with=ก')"/></xsl:attribute>
					<xsl:text>ก</xsl:text>
				</a>
			</li>
			<li>
				<a class="">
					<xsl:attribute name="href"><xsl:value-of select="concat($base_url, '&amp;starts_with=ข')"/></xsl:attribute>
					<xsl:text>ข</xsl:text>
				</a>
			</li>
			<li>
				<a class ="">
					<xsl:attribute name="href"><xsl:value-of select="concat($base_url, '&amp;starts_with=ค')"/></xsl:attribute>
					<xsl:text>ค</xsl:text>
				</a>
			</li>
			<li>
				<a class="">
					<xsl:attribute name="href"><xsl:value-of select="concat($base_url, '&amp;starts_with=ฆ')"/></xsl:attribute>
					<xsl:text>ฆ</xsl:text>
				</a>
			</li>
			<li>
				<a class="">
					<xsl:attribute name="href"><xsl:value-of select="concat($base_url, '&amp;starts_with=ง')"/></xsl:attribute>
					<xsl:text>ง</xsl:text>
				</a>
			</li>
			<li>
				<a class="">
					<xsl:attribute name="href"><xsl:value-of select="concat($base_url, '&amp;starts_with=จ')"/></xsl:attribute>
					<xsl:text>จ</xsl:text>
				</a>
			</li>
			<li>
				<a class="">
					<xsl:attribute name="href"><xsl:value-of select="concat($base_url, '&amp;starts_with=ฉ')"/></xsl:attribute>
					<xsl:text>ฉ</xsl:text>
				</a>
			</li>
			<li>
				<a class="">
					<xsl:attribute name="href"><xsl:value-of select="concat($base_url, '&amp;starts_with=ช')"/></xsl:attribute>
					<xsl:text>ช</xsl:text>
				</a>
			</li>
			<li>
				<a class="">
					<xsl:attribute name="href"><xsl:value-of select="concat($base_url, '&amp;starts_with=ซ')"/></xsl:attribute>
					<xsl:text>ซ</xsl:text>
				</a>
			</li>
			<li>
				<a class="">
					<xsl:attribute name="href"><xsl:value-of select="concat($base_url, '&amp;starts_with=ฌ')"/></xsl:attribute>
					<xsl:text>ฌ</xsl:text>
				</a>
			</li>
			<li>
				<a class="">
					<xsl:attribute name="href"><xsl:value-of select="concat($base_url, '&amp;starts_with=ญ')"/></xsl:attribute>
					<xsl:text>ญ</xsl:text>
				</a>
			</li>
			<li>
				<a class="">
					<xsl:attribute name="href"><xsl:value-of select="concat($base_url, '&amp;starts_with=ฎ')"/></xsl:attribute>
					<xsl:text>ฎ</xsl:text>
				</a>
			</li>
			<li>
				<a class="">
					<xsl:attribute name="href"><xsl:value-of select="concat($base_url, '&amp;starts_with=ฏ')"/></xsl:attribute>
					<xsl:text>ฏ</xsl:text>
				</a>
			</li>
			<li>
				<a class="">
					<xsl:attribute name="href"><xsl:value-of select="concat($base_url, '&amp;starts_with=ฐ')"/></xsl:attribute>
					<xsl:text>ฐ</xsl:text>
				</a>
			</li>
			<li>
				<a class="">
					<xsl:attribute name="href"><xsl:value-of select="concat($base_url, '&amp;starts_with=ฑ')"/></xsl:attribute>
					<xsl:text>ฑ</xsl:text>
				</a>
			</li>
			<li>
				<a class="">
					<xsl:attribute name="href"><xsl:value-of select="concat($base_url, '&amp;starts_with=ฒ')"/></xsl:attribute>
					<xsl:text>ฒ</xsl:text>
				</a>
			</li>
			<li>
				<a class="">
					<xsl:attribute name="href"><xsl:value-of select="concat($base_url, '&amp;starts_with=ณ')"/></xsl:attribute>
					<xsl:text>ณ</xsl:text>
				</a>
			</li>
			<li>
				<a class="">
					<xsl:attribute name="href"><xsl:value-of select="concat($base_url, '&amp;starts_with=ด')"/></xsl:attribute>
					<xsl:text>ด</xsl:text>
				</a>
			</li>
			<li>
				<a class="">
					<xsl:attribute name="href"><xsl:value-of select="concat($base_url, '&amp;starts_with=ต')"/></xsl:attribute>
					<xsl:text>ต</xsl:text>
				</a>
			</li>
			<li>
				<a class="">
					<xsl:attribute name="href"><xsl:value-of select="concat($base_url, '&amp;starts_with=ถ')"/></xsl:attribute>
					<xsl:text>ถ</xsl:text>
				</a>
			</li>
			<li>
				<a class="">
					<xsl:attribute name="href"><xsl:value-of select="concat($base_url, '&amp;starts_with=ท')"/></xsl:attribute>
					<xsl:text>ท</xsl:text>
				</a>
			</li>
			<li>
				<a class="">
					<xsl:attribute name="href"><xsl:value-of select="concat($base_url, '&amp;starts_with=ธ')"/></xsl:attribute>
					<xsl:text>ธ</xsl:text>
				</a>
			</li>
			<li>
				<a class="">
					<xsl:attribute name="href"><xsl:value-of select="concat($base_url, '&amp;starts_with=น')"/></xsl:attribute>
					<xsl:text>น</xsl:text>
				</a>
			</li>
			<li>
				<a class="">
					<xsl:attribute name="href"><xsl:value-of select="concat($base_url, '&amp;starts_with=บ')"/></xsl:attribute>
					<xsl:text>บ</xsl:text>
				</a>
			</li>
			<li>
				<a class="">
					<xsl:attribute name="href"><xsl:value-of select="concat($base_url, '&amp;starts_with=ป')"/></xsl:attribute>
					<xsl:text>ป</xsl:text>
				</a>
			</li>
			<li>
				<a class="">
					<xsl:attribute name="href"><xsl:value-of select="concat($base_url, '&amp;starts_with=ผ')"/></xsl:attribute>
					<xsl:text>ผ</xsl:text>
				</a>
			</li>
			<li>
				<a class="">
					<xsl:attribute name="href"><xsl:value-of select="concat($base_url, '&amp;starts_with=ฝ')"/></xsl:attribute>
					<xsl:text>ฝ</xsl:text>
				</a>
			</li>
			<li>
				<a class="">
					<xsl:attribute name="href"><xsl:value-of select="concat($base_url, '&amp;starts_with=พ')"/></xsl:attribute>
					<xsl:text>พ</xsl:text>
				</a>
			</li>
			<li>
				<a class="">
					<xsl:attribute name="href"><xsl:value-of select="concat($base_url, '&amp;starts_with=ฟ')"/></xsl:attribute>
					<xsl:text>ฟ</xsl:text>
				</a>
			</li>
			<li>
				<a class="">
					<xsl:attribute name="href"><xsl:value-of select="concat($base_url, '&amp;starts_with=ภ')"/></xsl:attribute>
					<xsl:text>ภ</xsl:text>
				</a>
			</li>
			<li>
				<a class="">
					<xsl:attribute name="href"><xsl:value-of select="concat($base_url, '&amp;starts_with=ม')"/></xsl:attribute>
					<xsl:text>ม</xsl:text>
				</a>
			</li>
			<li>
				<a class="">
					<xsl:attribute name="href"><xsl:value-of select="concat($base_url, '&amp;starts_with=ย')"/></xsl:attribute>
					<xsl:text>ย</xsl:text>
				</a>
			</li>
			<li>
				<a class="">
					<xsl:attribute name="href"><xsl:value-of select="concat($base_url, '&amp;starts_with=ร')"/></xsl:attribute>
					<xsl:text>ร</xsl:text>
				</a>
			</li>
			<li>
				<a class="">
					<xsl:attribute name="href"><xsl:value-of select="concat($base_url, '&amp;starts_with=ล')"/></xsl:attribute>
					<xsl:text>ล</xsl:text>
				</a>
			</li>
			<li>
				<a class="">
					<xsl:attribute name="href"><xsl:value-of select="concat($base_url, '&amp;starts_with=ว')"/></xsl:attribute>
					<xsl:text>ว</xsl:text>
				</a>
			</li>
			<li>
				<a class="">
					<xsl:attribute name="href"><xsl:value-of select="concat($base_url, '&amp;starts_with=ศ')"/></xsl:attribute>
					<xsl:text>ศ</xsl:text>
				</a>
			</li>
			<li>
				<a class="">
					<xsl:attribute name="href"><xsl:value-of select="concat($base_url, '&amp;starts_with=ษ')"/></xsl:attribute>
					<xsl:text>ษ</xsl:text>
				</a>
			</li>
			<li>
				<a class="">
					<xsl:attribute name="href"><xsl:value-of select="concat($base_url, '&amp;starts_with=ส')"/></xsl:attribute>
					<xsl:text>ส</xsl:text>
				</a>
			</li>
			<li>
				<a class="">
					<xsl:attribute name="href"><xsl:value-of select="concat($base_url, '&amp;starts_with=ห')"/></xsl:attribute>
					<xsl:text>ห</xsl:text>
				</a>
			</li>
			<li>
				<a class="">
					<xsl:attribute name="href"><xsl:value-of select="concat($base_url, '&amp;starts_with=ฬ')"/></xsl:attribute>
					<xsl:text>ฬ</xsl:text>
				</a>
			</li>
			<li>
				<a class="">
					<xsl:attribute name="href"><xsl:value-of select="concat($base_url, '&amp;starts_with=อ')"/></xsl:attribute>
					<xsl:text>อ</xsl:text>
				</a>
			</li>
			<li>
				<a class="">
					<xsl:attribute name="href"><xsl:value-of select="concat($base_url, '&amp;starts_with=ฮ')"/></xsl:attribute>
					<xsl:text>ฮ</xsl:text>
				</a>
			</li>
		</div>
        </ul>		
	</xsl:template>
	
</xsl:stylesheet>
