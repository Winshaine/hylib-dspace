<?xml version="1.0" encoding="UTF-8"?>
<!--

    The contents of this file are subject to the license and copyright
    detailed in the LICENSE and NOTICE files at the root of the source
    tree and available online at

    http://www.dspace.org/license/

-->
<!--
    TODO: Describe this XSL file
    Author: Alexey Maslov

-->

<xsl:stylesheet xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
	xmlns:dri="http://di.tamu.edu/DRI/1.0/"
	xmlns:mets="http://www.loc.gov/METS/"
	xmlns:xlink="http://www.w3.org/TR/xlink/"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
	xmlns:dim="http://www.dspace.org/xmlns/dspace/dim"
	xmlns:xhtml="http://www.w3.org/1999/xhtml"
	xmlns:mods="http://www.loc.gov/mods/v3"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="i18n dri mets xlink xsl dim xhtml mods dc">

    <xsl:import href="../dri2xhtml-alt/dri2xhtml.xsl"/>
    <xsl:import href="lib/xsl/core/global-variables.xsl"/>
    <xsl:import href="lib/xsl/core/page-structure.xsl"/>
    <xsl:import href="lib/xsl/core/navigation.xsl"/>
    <xsl:import href="lib/xsl/core/elements.xsl"/>
    <xsl:import href="lib/xsl/core/forms.xsl"/>
    <xsl:import href="lib/xsl/core/attribute-handlers.xsl"/>
    <xsl:import href="lib/xsl/core/utils.xsl"/>
    <xsl:import href="lib/xsl/aspect/general/choice-authority-control.xsl"/>
    <xsl:import href="lib/xsl/aspect/administrative/administrative.xsl"/>
    <xsl:import href="lib/xsl/aspect/artifactbrowser/item-list.xsl"/>
    <xsl:import href="lib/xsl/aspect/artifactbrowser/item-view.xsl"/>
    <xsl:import href="lib/xsl/aspect/artifactbrowser/community-list.xsl"/>
    <xsl:import href="lib/xsl/aspect/artifactbrowser/collection-list.xsl"/>
	<!-- HyLib Extensions Import -->
	<xsl:import href="../hylib-extensions/mediaplayer.xsl"/>
	<xsl:import href="../hylib-extensions/dateformat.xsl"/>
	<xsl:import href="../hylib-extensions/browsetitle.xsl"/>
	<xsl:import href="../hylib-extensions/remotesearch.xsl"/>

    <xsl:output indent="yes"/>
    
	<xsl:param name="locale"/>

    <!-- Some custom template -->
    <xsl:template match="dri:body/dri:div/dri:head">
        <xsl:call-template name="renderHead">
            <xsl:with-param name="class">ds-div-head</xsl:with-param>
        </xsl:call-template>
        <div id="community-list">
            <table class="community-table">
                <xsl:call-template name="community-cell"/>
            </table>
        </div>
    </xsl:template>

    <xsl:variable name="rowNumber">
       <xsl:value-of select="ceiling(count(//dri:reference[@type='DSpace Community']//dri:reference) div 2)"/>
    </xsl:variable>

    <xsl:template name="community-cell">
                <xsl:param name="index" select="1"/>
                <xsl:param name="node" select="1"/>
                <tr>
                        <td class="community-cell"><xsl:apply-templates select="//dri:reference[@type='DSpace Community']//dri:reference[$node]" mode="detailList" /></td>
                        <td class="community-cell"><xsl:apply-templates select="//dri:reference[@type='DSpace Community']//dri:reference[$node+1]" mode="detailList"/></td>
                </tr>
                <xsl:if test="$index &lt; $rowNumber">
                        <xsl:call-template name="community-cell">
                                <xsl:with-param name="index" select="$index + 1"/>
                                <xsl:with-param name="node" select="$node + 2"/>
                        </xsl:call-template>
                </xsl:if>
    </xsl:template>


</xsl:stylesheet>
