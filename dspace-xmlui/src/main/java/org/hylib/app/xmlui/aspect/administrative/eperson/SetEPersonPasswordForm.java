package org.hylib.app.xmlui.aspect.administrative.eperson;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import org.apache.avalon.framework.parameters.Parameters;
import org.apache.cocoon.ProcessingException;
import org.apache.cocoon.environment.SourceResolver;
import org.dspace.app.xmlui.cocoon.AbstractDSpaceTransformer;
import org.dspace.app.xmlui.utils.UIException;
import org.dspace.app.xmlui.wing.Message;
import org.dspace.app.xmlui.wing.WingException;
import org.dspace.app.xmlui.wing.element.Body;
import org.dspace.app.xmlui.wing.element.Division;
import org.dspace.app.xmlui.wing.element.Item;
import org.dspace.app.xmlui.wing.element.List;
import org.dspace.app.xmlui.wing.element.PageMeta;
import org.dspace.app.xmlui.wing.element.Password;
import org.dspace.authorize.AuthorizeException;
import org.dspace.authorize.AuthorizeManager;
import org.dspace.eperson.EPerson;
import org.dspace.eperson.Group;
import org.xml.sax.SAXException;

/**
 * Edit an existing EPerson, display all the eperson's metadata along with two
 * special options two reset the eperson's password and delete this user.
 * 
 * @author Alexey Maslov
 */
public class SetEPersonPasswordForm extends AbstractDSpaceTransformer
{
    /** Language Strings */
    private static final Message T_dspace_home = message("xmlui.general.dspace_home");

    private static final Message T_submit_save = message("xmlui.general.save");

    private static final Message T_submit_cancel = message("xmlui.general.cancel");

    private static final Message T_title = message("xmlui.administrative.eperson.SetEPersonPasswordForm.title");

    private static final Message T_eperson_trail = message("xmlui.administrative.eperson.general.epeople_trail");

    private static final Message T_trail = message("xmlui.administrative.eperson.SetEPersonPasswordForm.trail");

    private static final Message T_head1 = message("xmlui.administrative.eperson.SetEPersonPasswordForm.head1");

    private static final Message T_head2 = message("xmlui.administrative.eperson.SetEPersonPasswordForm.head2");

    private static final Message T_member_head = message("xmlui.administrative.eperson.EditEPersonForm.member_head");

    private static final Message T_indirect_member = message("xmlui.administrative.eperson.EditEPersonForm.indirect_member");

    private static final Message T_member_none = message("xmlui.administrative.eperson.EditEPersonForm.member_none");

    /** Language string used: */

    private static final Message T_password = message("xmlui.EPerson.EditProfile.password");

    private static final Message T_error_invalid_password = message("xmlui.EPerson.EditProfile.error_invalid_password");

    private static final Message T_confirm_password = message("xmlui.EPerson.EditProfile.confirm_password");

    private static final Message T_error_unconfirmed_password = message("xmlui.EPerson.EditProfile.error_unconfirmed_password");

    /** A list of fields in error */
    private java.util.List<String> errors;

    public void setup(SourceResolver resolver, Map objectModel, String src,
            Parameters parameters) throws ProcessingException, SAXException,
            IOException
    {
        super.setup(resolver, objectModel, src, parameters);

        String errors = parameters.getParameter("errors", "");
        if (errors.length() > 0)
            this.errors = Arrays.asList(errors.split(","));
        else
            this.errors = new ArrayList<String>();
    }

    public void addPageMeta(PageMeta pageMeta) throws WingException
    {
        pageMeta.addMetadata("title").addContent(T_title);
        pageMeta.addTrailLink(contextPath + "/", T_dspace_home);
        pageMeta.addTrailLink(contextPath + "/admin/epeople", T_eperson_trail);
        pageMeta.addTrail().addContent(T_trail);
    }

    public void addBody(Body body) throws WingException, SQLException,
            AuthorizeException
    {
        // Get all our parameters
        boolean admin = AuthorizeManager.isAdmin(context);

        // Get our parameters;
        int epersonID = parameters.getParameterAsInteger("epersonID", -1);

        // Grab the person in question
        EPerson eperson = EPerson.find(context, epersonID);

        if (eperson == null)
            throw new UIException("Unable to find eperson for id:" + epersonID);

        // DIVISION: eperson-edit
        Division edit = body.addInteractiveDivision("eperson-edit", contextPath
                + "/admin/epeople", Division.METHOD_POST,
                "primary administrative eperson");
        edit.setHead(T_head1);

        List identity = edit.addList("form", List.TYPE_FORM);
        identity.setHead(T_head2.parameterize(eperson.getFullName()));

        if (admin)
        {
            Password password = identity.addItem().addPassword("password");
            password.setRequired();
            password.setLabel(T_password);

            if (errors.contains("password"))
            {
                password.addError(T_error_invalid_password);
            }
        }
        else
        {
            identity.addLabel(T_password);
            identity.addItem("");
        }

        if (admin)
        {
            Password passwordConfirm = identity.addItem().addPassword(
                    "password_confirm");
            passwordConfirm.setRequired();
            passwordConfirm.setLabel(T_confirm_password);

            if (errors.contains("password_confirm"))
            {
                passwordConfirm.addError(T_error_unconfirmed_password);
            }
        }
        else
        {
            identity.addLabel(T_confirm_password);
            identity.addItem("");
        }

        Item buttons = identity.addItem();
        if (admin)
            buttons.addButton("submit_save").setValue(T_submit_save);
        buttons.addButton("submit_cancel").setValue(T_submit_cancel);

        if (admin)
        {
            List member = edit.addList("eperson-member-of");
            member.setHead(T_member_head);

            Group[] groups = Group.allMemberGroups(context, eperson);
            for (Group group : groups)
            {
                String url = contextPath
                        + "/admin/groups?administrative-continue="
                        + knot.getId() + "&submit_edit_group&groupID="
                        + group.getID();

                Item item = member.addItem();
                item.addXref(url, group.getName());

                // Check if this membership is via another group or not, if so
                // then add a note.
                Group via = findViaGroup(eperson, group);
                if (via != null)
                    item.addHighlight("fade").addContent(
                            T_indirect_member.parameterize(via.getName()));

            }

            if (groups.length <= 0)
                member.addItem().addHighlight("italic").addContent(
                        T_member_none);

        }

        edit.addHidden("administrative-continue").setValue(knot.getId());
    }

    /**
     * Determine if the given eperson is a direct member of this group if they
     * are not the return the group that membership is implied through (the via
     * group!). This will only find one possible relation path, there may be
     * multiple.
     * 
     * 
     * @param eperson
     *            The source group to search from
     * @param group
     *            The target group to search for.
     * @return The group this member is related through or null if none found.
     */
    private Group findViaGroup(EPerson eperson, Group group)
            throws SQLException
    {
        // First check if this eperson is a direct member of the group.
        for (EPerson direct : group.getMembers())
        {
            if (direct.getID() == eperson.getID())
                // Direct membership
                return null;
        }

        // Otherwise check what group this eperson is a member through
        Group[] targets = group.getMemberGroups();

        Group[] groups = Group.allMemberGroups(context, eperson);
        for (Group member : groups)
        {
            for (Group target : targets)
            {
                if (member.getID() == target.getID())
                    return member;
            }
        }

        // This should never happen, but let's just say we couldn't find the
        // relationship.
        return null;
    }

    /**
     * Recycle
     */
    public void recycle()
    {
        errors = null;
        super.recycle();
    }
}
