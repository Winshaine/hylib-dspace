package org.hylib.app.xmlui.aspect.artifactbrowser;

import java.util.ArrayList;

import org.dspace.app.xmlui.wing.WingException;
import org.dspace.app.xmlui.wing.element.Division;
import org.dspace.app.xmlui.wing.element.Row;
import org.dspace.app.xmlui.wing.element.Table;
import org.hylib.remotesearch.RemoteSearchServer;
import org.hylib.remotesearch.SRWRecord;
import org.hylib.remotesearch.SRWResponse;
import org.hylib.util.DateUtil;

public class ResultRenderUtil
{
    /**
     * Receives result (from one server) and division as parameters and render the
     * result table in the provided division.
     * 
     * @param div - division to render result table in
     * @param result - result used for rendering
     * @throws WingException 
     */
    public static void renderResultTable(Division div, SRWResponse result) throws WingException
    {
        RemoteSearchServer server = result.getServer();
        ArrayList<SRWRecord> records = result.getRecords();

        int numResult = records.size(); //to display
        
        //display the name of the server along with the number of results
        div.addDivision("search-result-header").addPara(server.getServerName() + " - " + result.getRecordCount() + " result(s)");
        
        if(numResult!=0)
        {
            //create table for this server
            Table t = div.addTable("rmsearch-result-table", numResult + 2, 4);
            
            //search result section
            for(int j=0; j<numResult; j++)
            {
                SRWRecord record = records.get(j);
                
                Row row = t.addRow("searchresult-row", Row.ROLE_DATA, null);
                row.addCell().addXref(record.getUrl().toString(), record.getTitle());
                String[] authors = record.getAuthors();
                String authorString = "";
                for(int k=0; k<authors.length; k++)
                {
                    authorString += authors[k];
                    
                    if(k<authors.length-1)
                        authorString += "; ";
                }
                row.addCellContent(authorString);
                row.addCellContent(record.getAbstractText());
                row.addCellContent(DateUtil.getDateString(record.getIssuedDate()));
            }
        }
    }
}
