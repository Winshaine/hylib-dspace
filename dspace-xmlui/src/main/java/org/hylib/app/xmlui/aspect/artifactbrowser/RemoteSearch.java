/*
 * RemoteSearch.java
 * 
 * Generate a DRI document for remote search page.
 * The document is very similar to an advanced search page
 * except that it has no scope but instead will have several
 * check boxes for the remote servers to send the search
 * request to.
 * 
 */
package org.hylib.app.xmlui.aspect.artifactbrowser;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.cocoon.caching.CacheableProcessingComponent;
import org.apache.cocoon.environment.ObjectModelHelper;
import org.apache.cocoon.environment.Request;
import org.apache.cocoon.util.HashUtil;
import org.dspace.app.xmlui.aspect.artifactbrowser.AbstractSearch;
import org.dspace.app.xmlui.cocoon.AbstractDSpaceTransformer;
import org.dspace.app.xmlui.utils.HandleUtil;
import org.dspace.app.xmlui.utils.UIException;
import org.dspace.app.xmlui.wing.Message;
import org.dspace.app.xmlui.wing.WingException;
import org.dspace.app.xmlui.wing.element.Body;
import org.dspace.app.xmlui.wing.element.Cell;
import org.dspace.app.xmlui.wing.element.CheckBox;
import org.dspace.app.xmlui.wing.element.Division;
import org.dspace.app.xmlui.wing.element.List;
import org.dspace.app.xmlui.wing.element.PageMeta;
import org.dspace.app.xmlui.wing.element.Para;
import org.dspace.app.xmlui.wing.element.Row;
import org.dspace.app.xmlui.wing.element.Select;
import org.dspace.app.xmlui.wing.element.Table;
import org.dspace.app.xmlui.wing.element.Text;
import org.dspace.authorize.AuthorizeException;
import org.dspace.content.Collection;
import org.dspace.content.Community;
import org.dspace.content.DSpaceObject;
import org.dspace.core.ConfigurationManager;
import org.hylib.remotesearch.RemoteSearchManager;
import org.hylib.remotesearch.RemoteSearchOperation;
import org.hylib.remotesearch.RemoteSearchServer;
import org.hylib.remotesearch.SRWResponse;
import org.hylib.util.LuceneIndexMapper;
import org.xml.sax.SAXException;

public class RemoteSearch extends AbstractSearch implements CacheableProcessingComponent
{

	/** Language string used: */
    private static final Message T_title =
        message("xmlui.ArtifactBrowser.RemoteSearch.title");
    
    private static final Message T_dspace_home =
        message("xmlui.general.dspace_home");
    
    private static final Message T_trail = 
        message("xmlui.ArtifactBrowser.RemoteSearch.trail");
    
    private static final Message T_head = 
        message("xmlui.ArtifactBrowser.RemoteSearch.head");
    
    private static final Message T_search_type =
        message("xmlui.ArtifactBrowser.RemoteSearch.search_type");
    
    private static final Message T_search_for = 
        message("xmlui.ArtifactBrowser.RemoteSearch.search_for");
    
    private static final Message T_go = 
        message("xmlui.general.go");

    private static final Message T_and = 
        message("xmlui.ArtifactBrowser.RemoteSearch.and");
    
    private static final Message T_or = 
        message("xmlui.ArtifactBrowser.RemoteSearch.or");
    
	private static final Message T_not = 
		message("xmlui.ArtifactBrowser.RemoteSearch.not");
	
    private static final Message T_rmsearch_label = 
        message("xmlui.ArtifactBrowser.RemoteSearch.rmsearch_label");
    
    private static final Message T_rmsearch_help = 
        message("xmlui.ArtifactBrowser.RemoteSearch.rmsearch_help");
    
    private static final Message T_viewall = 
        message("xmlui.ArtifactBrowser.RemoteSearch.rmsearch_viewall");
    
    private static final Message T_serverdown = 
        message("xmlui.ArtifactBrowser.RemoteSearch.rmsearch_serverdown");
	
        
	/** How many conjunction fields to display */
    private static final int FIELD_DISPLAY_COUNT = 3;
    private static final int FIELD_MAX_COUNT = 12;
    
    /** A cache of extracted search fields */
    private java.util.List<SearchField> fields;
    
    /**
     * Generate the unique caching key.
     * This key must be unique inside the space of this component.
     * 
     * Added (from original): need to take into account the checked servers too
     */
    public Serializable getKey()
    {        
        try 
        {
            String key = "";
            
            // Page Parameter
            key += "-" + getParameterPage();
            key += "-" + getParameterEtAl();
            
            // The actual search query.
            key += "-" + getQuery();
            
            // checked servers
            key += "-" + getServers();

            return HashUtil.hash(key);
        }
        catch (Exception e)
        {
            // Ignore all errors and just don't cache.
            return "0";
        }
    }
    
    /**
     * Add Page metadata.
     */
    public void addPageMeta(PageMeta pageMeta) throws WingException, SQLException
    {
        pageMeta.addMetadata("title").addContent(T_title);
        pageMeta.addTrailLink(contextPath + "/", T_dspace_home);
        
		DSpaceObject dso = HandleUtil.obtainHandle(objectModel);
        if ((dso instanceof Collection) || (dso instanceof Community))
        {
	        HandleUtil.buildHandleTrail(dso,pageMeta,contextPath);
		} 
		
        pageMeta.addTrail().addContent(T_trail);
    }   
    
    /**
     * Add the body
     */
    public void addBody(Body body) throws SAXException, WingException,
            UIException, SQLException, IOException, AuthorizeException
    {
        // Build the DRI Body
        Division search = body.addDivision("remote-search","primary");
        search.setHead(T_head);
        search.addPara().addContent("");
        Division query = search.addInteractiveDivision("search-query",
                "remote-search",Division.METHOD_POST,"secondary search");
        
        Table queryTable = query.addTable("search-query", 4, 3);
        Row header = queryTable.addRow(Row.ROLE_HEADER);
        header.addCellContent("");
        header.addCellContent(T_search_type);
        header.addCellContent(T_search_for);
        
        for (int i = 1; i <= FIELD_DISPLAY_COUNT; i++)
        {
            Row row = queryTable.addRow(Row.ROLE_DATA);
            buildConjunctionField(i, row.addCell());
            buildTypeField(i, row.addCell());
            buildQueryField(i, row.addCell());
        }

        for (SearchField field : fields)
        {
        	// Skip over all the fields we've displayed.
        	int i = field.getIndex();
        	if (i <= FIELD_DISPLAY_COUNT)
        		continue;
        	
        	query.addHidden("conjunction"+i).setValue(field.getConjunction());
        	query.addHidden("field"+i).setValue(field.getField());
        	query.addHidden("query"+i).setValue(field.getQuery());
        }

        //buildSearchControls(query);
        
        // Add checkboxes for remote search servers
        buildRMServCheckboxes(query);
        
        query.addPara(null, "button-list").addButton("submit").setValue(T_go);
        
        // Add the result division
        buildSearchResultsDivision(search);
    }
    
    /**
     * Build a conjunction field in the given for the given cell. A 
     * conjunction consists of logical the operators AND, OR, NOT.
     *
     * @param row The current row
     * @param cell The current cell
     */
    private void buildConjunctionField(int row, Cell cell) throws WingException
    {
        // No conjunction for the first row.
        if (row == 1)
        {
            return;
        }

        Request request = ObjectModelHelper.getRequest(objectModel);
        String current = request.getParameter("conjunction" + row);

        // default to AND if nothing specified.
        if (current == null || current.length() == 0)
        {
            current = "AND";
        }
        
        Select select = cell.addSelect("conjunction" + row);

        select.addOption("AND".equals(current), "AND").addContent(T_and);
        select.addOption("OR".equals(current), "OR").addContent(T_or);
        select.addOption("NOT".equals(current), "NOT").addContent(T_not);
    }
    
    /**
     * Build a list of all the indexable fields in the given cell.
     * 
     * FIXME: This needs to use the dspace.cfg data
     * 
     * @param row The current row
     * @param cell The current cell
     */
    private void buildTypeField(int row, Cell cell) throws WingException
    {
        Request request = ObjectModelHelper.getRequest(objectModel);
        String current = request.getParameter("field" + row);
        
        Select select = cell.addSelect("field" + row);

        Map<String, Message> searchTypes = new HashMap<String, Message>();
        
        int i = 1;
        String sindex = ConfigurationManager.getProperty("search.index." + i);
        while(sindex != null)
        {
            String field = sindex.split(":")[0];               
            searchTypes.put(field, message("xmlui.ArtifactBrowser.AdvancedSearch.type_" + field));
            
            sindex = ConfigurationManager.getProperty("search.index." + ++i);
        }
            
        
        
        // Special case ANY
        select.addOption((current == null), "ANY").addContent(
                message("xmlui.ArtifactBrowser.AdvancedSearch.type_ANY"));

        for (Map.Entry<String, Message> searchType : searchTypes.entrySet())
        {
            select.addOption(searchType.getKey().equals(current), searchType.getKey()).addContent(searchType.getValue());
        }
    }
    
    /**
     * Recycle
     */
    public void recycle() 
    {
        this.fields = null;
        super.recycle();
    }
    
    /**
     * Build the remote server check boxes
     * 
     * @throws WingException 
     */
    private void buildRMServCheckboxes(Division query) throws WingException
    {        
        java.util.List<RemoteSearchServer> serverlist = RemoteSearchManager.getServerlist();
               
        List form = query.addList("remote-search",List.TYPE_FORM);
        
        //add check box form for this server
        CheckBox checkbox = form.addItem().addCheckBox("remote-search-server");
        checkbox.setLabel(T_rmsearch_label);
        checkbox.setHelp(T_rmsearch_help);
        
        //get previously checked servers
        ArrayList<String> checkedserv = getServerList();
        
        for (int i = 0; i < serverlist.size(); i++)
        {   
            //get object
            RemoteSearchServer rmobject = serverlist.get(i);

            //add checkbox
            checkbox.addOption(rmobject.getServerName(),rmobject.getServerDescription());
            
            if(checkedserv.contains(rmobject.getServerName()))
                checkbox.setOptionSelected(rmobject.getServerName());
        }
    }
    
    /**
     * Build the query field for the given cell.
     * 
     * @param row The current row.
     * @param cell The current cell.
     */
    private void buildQueryField(int row, Cell cell) throws WingException
    {
        Request request = ObjectModelHelper.getRequest(objectModel);
        String current = decodeFromURL(request.getParameter("query" + row));

        Text text = cell.addText("query" + row);
        if (current != null)
        {
            text.setValue(current);
        }
    }
    
    /**
     * Generate a URL for this search page which includes all the 
     * search parameters along with the added parameters.
     * 
     * @param parameters URL parameters to be included in the generated url.
     */
	protected String generateURL(Map<String, String> parameters)
			throws UIException {
	            
	    return null;
	}

	/**
     * Determine the search query for this search page.
     * 
     * @return the query.
     */
    protected String getQuery() throws UIException
    {
        Request request = ObjectModelHelper.getRequest(objectModel);   
        return buildQuery(getSearchFields(request));
    }
    
    /**
     * Determine the checked servers
     * 
     * @return server string
     */
    private String getServers()
    {
        String serverString = "";
        
        Request request = ObjectModelHelper.getRequest(objectModel); 
        String[] serverCheckboxes = request.getParameterValues("remote-search-server");
        
        if (serverCheckboxes != null)
        {
            for (String checkbox : serverCheckboxes)
            {
                serverString += checkbox;
            }
        }
        
        return serverString;
    }
	
    /**
     * Get a list of search fields from the request object
     * and parse them into a linear array of fileds. The field's
     * index is preserved, so if it comes in as index 17 it will 
     * be outputted as field 17.
     * 
     * @param request The http request object
     * @return Array of search fields
     * @throws UIException 
     */
    public java.util.List<SearchField> getSearchFields(Request request) throws UIException
	{
    	if (this.fields != null)
        {
            return this.fields;
        }
    	
    	// Get how many fields to search
	    int numSearchField;
	    try {
	    	String numSearchFieldStr = request.getParameter("num_search_field");
	    	numSearchField = Integer.valueOf(numSearchFieldStr);
	    } 
	    catch (NumberFormatException nfe)
	    {
	    	numSearchField = FIELD_MAX_COUNT;
	    }
	    	
    	// Iterate over all the possible fields and add each one to the list of fields.
		ArrayList<SearchField> fields = new ArrayList<SearchField>();
		for (int i = 1; i <= numSearchField; i++)
		{
			String field = request.getParameter("field"+i);
			String query = decodeFromURL(request.getParameter("query"+i));
			String conjunction = request.getParameter("conjunction"+i);
			
			if (field != null)
			{
				field = field.trim();
				if (field.length() == 0)
                {
                    field = null;
                }
			}
			
			
			if (query != null)
			{
				query = query.trim();
				if (query.length() == 0)
                {
                    query = null;
                }
			}
			
			if (conjunction != null)
			{
				conjunction = conjunction.trim();
				if (conjunction.length() == 0)
                {
                    conjunction = null;
                }
			}
			
			if (field == null)
            {
                field = "ANY";
            }
			if (conjunction == null)
            {
                conjunction = "AND";
            }
			
			if (query != null)
            {
                fields.add(new SearchField(i, field, query, conjunction));
            }
		}
		
		this.fields = fields;
		
		return this.fields;
	}
    
	/**
     * Given a list of search fields build a CQL search query string.
     * 
     * @param fields The search fields
     * @return A string
     */
    private String buildQuery(java.util.List<SearchField> fields)
    {        
        String query = "";
        
        // Loop through the fields building the search query as we go.
        for (SearchField field : fields)
        {   
            // if the field is empty, then skip it and try a later one.
            if (field.getQuery() == null)
                continue;
            
            // Add the conjunction for everything but the first field.
            if (fields.indexOf(field) > 0)
                query += " " + field.getConjunction() + " ";
            
            // Get lucene index from the given search type (strip the .* out from the end of the string)          
            String luceneIndex = LuceneIndexMapper.getLuceneFromSearchType(field.getField());

            // Create new search word for CQL (space must be replaced with plus sign)
            String search = "";
            String[] searchWords = field.getQuery().trim().split(" ");
            
            for(int i=0; i<searchWords.length; i++)
            {         
                if(searchWords[i].equals(""))
                    continue;
                    
                search += searchWords[i];
                
                if(i<searchWords.length-1)
                    search += "+";
            }
            
            if(luceneIndex!=null)
                query += luceneIndex + "=\"" + search + "\"";
        }
        
        if (query.length() == 0)
            return "";
        else
            return query;
    }
    
    /**
     * 
     * Attach a division to the given search division named "search-results"
     * which contains results for this search query.
     * 
     * @param search
     *            The search division to contain the search-results division.
     */
    protected void buildSearchResultsDivision(Division search)
            throws IOException, SQLException, WingException
    {
        //do nothing if we're not performing the search (first load of this page)
        if(getQuery().equals("") || getServerList().size()==0)
            return;
         
        //perform the search and retrieve the responses
        ArrayList<SRWResponse> responses = RemoteSearchOperation.performRemoteSearch(getQuery(), getServerList(), 1, FIELD_MAX_COUNT);
        
        //add a blank paragraph to separate the section
        search.addPara("");
       
        //display results for each server
        for(int i=0; i<responses.size(); i++)
        {
            SRWResponse response = responses.get(i);
            RemoteSearchServer server = response.getServer();
            
            // check if this server is not down
            if(response.isFunctioning())
            {                
                ResultRenderUtil.renderResultTable(search, response);
                
                if(responses.get(i).getRecordCount()>RemoteSearchManager.getResultPerPage())
                {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("server", encodeForURL(server.getServerName()));
                    params.put("query", encodeForURL(getQuery()));
                    params.put("page", "1");
                    params.put("recordNum", ""+response.getRecordCount());
                    
                    search.addPara().addXref(AbstractDSpaceTransformer.generateURL("remote-search/viewall", params), T_viewall);
                }
                
                search.addPara("");
            }
            else
            {
                Para p = search.addDivision("search-result-header").addPara();
                p.addContent(server.getServerDescription() + " - ");
                p.addContent(T_serverdown);
            }
        }
    }
    
    /**
     * Iterate over all check boxes and add checked servers into an array to be returned
     * 
     * @return server_array;
     */
    private ArrayList<String> getServerList()
    {
        ArrayList<String> serverArray = new ArrayList<String>();
        
        Request request = ObjectModelHelper.getRequest(objectModel);
        String[] serverCheckboxes = request.getParameterValues("remote-search-server");
        
        if (serverCheckboxes != null)
        {
            // only those checked will enter the for loop
            for (String checkbox : serverCheckboxes)
            {
                serverArray.add(checkbox);
            }
        }
        
        return serverArray;
    }
    
	/**
     * A private record keeping class to relate the various 
     * components of a search field together.
     */
    private static class SearchField {
    	
    	/** What index the search field is, typicaly there are just three - but the theme may exand this number */
    	private int index;
    	
    	/** The field to search, ANY if none specified */
    	private String field;
    	
    	/** The query string to search for */
    	private String query;
    	
    	/** the conjunction: either "AND" or "OR" */
    	private String conjuction;
    	
    	public SearchField(int index, String field, String query, String conjunction)
    	{
    		this.index = index;
    		this.field = field;
    		this.query = query;
    		this.conjuction = conjunction;
    	}
    	
    	public int    getIndex() { return this.index;}
    	public String getField() { return this.field;}
    	public String getQuery() { return this.query;}
    	public String getConjunction() { return this.conjuction;} 
    }
}
