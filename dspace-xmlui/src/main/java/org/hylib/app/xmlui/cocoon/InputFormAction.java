/*
 * InputFormAction.java
 *
 * Date: $Date: 2009-07-27 13:31:24 +0200 (Mon, 27 Jul 2009) $
 * Author: Kornschnok Dittawit
 *
 * Copyright (c) 2009, Asian Institute of Technology.  
 * All rights reserved.
 */

/**
 * Test to see if there's a specific input form for the specified handle.
 * If the returned form name is *, it means we may choose a specific form name for
 * this handle. Otherwise, we have to use the assigned form.
 */

package org.hylib.app.xmlui.cocoon;

import java.util.HashMap;
import java.util.Map;

import org.apache.avalon.framework.parameters.Parameters;
import org.apache.cocoon.acting.ServiceableAction;
import org.apache.cocoon.environment.Redirector;
import org.apache.cocoon.environment.SourceResolver;
import org.dspace.app.util.DCInputsReader;

public class InputFormAction extends ServiceableAction{

    /**
     * A shared resource of the inputs reader. The 'inputs' are the 
     * questions we ask the user to describe an item during the 
     * submission process. The reader is a utility class to read 
     * that configuration file.
     */
    public static DCInputsReader INPUTS_READER = null;
	
	public Map act(Redirector redirector,
			SourceResolver resolver,
			Map objectModel,
			String source,
			Parameters parameters)
	throws Exception {
		String formName = "*"; //default (just in case)
		
		// Get form name from the item id
		String handle = parameters.getParameter("handle");
		if (INPUTS_READER == null)
            INPUTS_READER = new DCInputsReader();
		
		formName = INPUTS_READER.getFormNameByCollection(handle);

		// Set up a map for sitemap parameters
		Map map = new HashMap();
		map.put("formname", formName);
		map.put("handle", handle);
		return map;
	}

}
