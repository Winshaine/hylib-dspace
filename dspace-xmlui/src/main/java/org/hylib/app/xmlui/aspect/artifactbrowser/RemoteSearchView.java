package org.hylib.app.xmlui.aspect.artifactbrowser;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.cocoon.caching.CacheableProcessingComponent;
import org.apache.cocoon.environment.ObjectModelHelper;
import org.apache.cocoon.environment.Request;
import org.apache.excalibur.source.SourceValidity;
import org.dspace.app.xmlui.cocoon.AbstractDSpaceTransformer;
import org.dspace.app.xmlui.utils.HandleUtil;
import org.dspace.app.xmlui.utils.UIException;
import org.dspace.app.xmlui.wing.Message;
import org.dspace.app.xmlui.wing.WingException;
import org.dspace.app.xmlui.wing.element.Body;
import org.dspace.app.xmlui.wing.element.Division;
import org.dspace.app.xmlui.wing.element.PageMeta;
import org.dspace.app.xmlui.wing.element.Para;
import org.dspace.authorize.AuthorizeException;
import org.dspace.content.Collection;
import org.dspace.content.Community;
import org.dspace.content.DSpaceObject;
import org.hylib.remotesearch.RemoteSearchManager;
import org.hylib.remotesearch.RemoteSearchOperation;
import org.hylib.remotesearch.RemoteSearchServer;
import org.hylib.remotesearch.SRWResponse;
import org.hylib.util.LuceneIndexMapper;
import org.xml.sax.SAXException;

public class RemoteSearchView extends AbstractDSpaceTransformer implements CacheableProcessingComponent
{
    /** Language string used: */
    private static final Message T_title =
        message("xmlui.ArtifactBrowser.RemoteSearchView.title");
    
    private static final Message T_dspace_home =
        message("xmlui.general.dspace_home");
    
    private static final Message T_trail = 
        message("xmlui.ArtifactBrowser.RemoteSearchView.trail");
    
    private static final Message T_head = 
        message("xmlui.ArtifactBrowser.RemoteSearchView.head");
    
    private static final Message T_query = 
        message("xmlui.ArtifactBrowser.RemoteSearchView.query");
    
    private static final Message T_server = 
        message("xmlui.ArtifactBrowser.RemoteSearchView.server");
    
    private static final Message T_numresult = 
        message("xmlui.ArtifactBrowser.RemoteSearchView.numresult");
    
    /** number of results to be displayed per page */
    private final int NUM_DISPLAY = 10;
    
    public Serializable getKey()
    {
        // TODO Auto-generated method stub
        return null;
    }

    public SourceValidity getValidity()
    {
        // TODO Auto-generated method stub
        return null;
    }
    
    public void addBody(Body body) throws SAXException, WingException,
        UIException, SQLException, IOException, AuthorizeException
    {
        Request request = ObjectModelHelper.getRequest(objectModel);
        String servname = request.getParameter("server");
        String query = request.getParameter("query");
        int page = Integer.parseInt(request.getParameter("page"));
        int recordNum = Integer.parseInt(request.getParameter("recordNum"));
        
        // get server object
        RemoteSearchServer server = RemoteSearchManager.getServerFromName(servname);
        
        // change all lucene indices in a query to normal search types for display purpose
        String queryDisplay = createDisplayQuery(query);
        
        // Build the DRI Body
        Division result = body.addDivision("remote-search-results","primary");
        result.setHead(T_head);
        Para p = result.addPara();
        p.addContent(T_query);
        p.addContent(" " + queryDisplay);
        Para p2 = result.addPara();
        p2.addContent(T_server);
        p2.addContent(" " + server.getServerDescription());
        Para p3 = result.addPara();
        p3.addContent(T_numresult);
        p3.addContent(" " + recordNum);
        
        // Build table containing the result
        buildResults(servname, query, page, recordNum, result);
    }
    
    public void addPageMeta(PageMeta pageMeta) throws SAXException,
        WingException, UIException, SQLException, IOException,
        AuthorizeException
    {
        pageMeta.addMetadata("title").addContent(T_title);
        pageMeta.addTrailLink(contextPath + "/", T_dspace_home);
        
        DSpaceObject dso = HandleUtil.obtainHandle(objectModel);
        if ((dso instanceof Collection) || (dso instanceof Community))
        {
            HandleUtil.buildHandleTrail(dso,pageMeta,contextPath);
        } 
        
        pageMeta.addTrail().addContent(T_trail);
    }
    
    /**
     * Build the result part
     * 
     * @param servername - server name to send the search request
     * @param query - search query
     * @param currentPage - requesting page
     * @param recordNum - actual number of records
     * @param div - division to build this part on
     * @throws WingException 
     */
    private void buildResults(String servername, String query, int currentPage, int recordNum, Division div) throws WingException
    {
        int numPage = getNumberOfPage(recordNum);
        int startRec = (currentPage-1)*NUM_DISPLAY + 1;
        
        ArrayList<String> servers = new ArrayList<String>();
        servers.add(servername);
        
        ArrayList<SRWResponse> responses = RemoteSearchOperation.performRemoteSearch(query, servers, startRec, NUM_DISPLAY);
        
        // add a blank paragraph to separate the section
        div.addPara("");
       
        // display result
        SRWResponse response = responses.get(0);
        
        // render result table
        ResultRenderUtil.renderResultTable(div, response);
        
        // build page number part
        Division pageDiv = div.addDivision("pagenumber");
        Para para = pageDiv.addPara();
        
        for(int j=1; j<=numPage; j++)
        {
            if(j==currentPage)
            {
                para.addContent(j);
            }
            else
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("server", encodeForURL(servername));
                params.put("query", encodeForURL(query));
                params.put("page", ""+j);
                params.put("recordNum", ""+recordNum);
                
                para.addXref(super.generateURL("viewall", params), ""+j);
            }
            para.addContent(" ");
        }            
        
    }
    
    /**
     * Get number of pages needed to display the specified number of records
     */
    private int getNumberOfPage(int recordNumber)
    {
       if(recordNumber%NUM_DISPLAY==0)
       {
           return recordNumber/NUM_DISPLAY;
       }
       else
       {
           return recordNumber/NUM_DISPLAY + 1;
       }
    }
    
    /**
     * Simplify the query by changing lucene indices to normal search types
     * 
     * @param query - query to change
     * @return a simpler query
     */
    private String createDisplayQuery(String query)
    {
    	ArrayList<String> luceneArray = LuceneIndexMapper.getLuceneArray();
    	
    	for(int i=0; i<luceneArray.size(); i++)
    	{
    		query = query.replace(luceneArray.get(i), LuceneIndexMapper.getSearchTypeFromLucene(luceneArray.get(i)));
    	}
    	
    	return query;
    }
}
