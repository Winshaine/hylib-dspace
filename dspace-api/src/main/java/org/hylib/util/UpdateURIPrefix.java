package org.hylib.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.dspace.browse.IndexBrowse;
import org.dspace.core.Context;
import org.dspace.search.DSIndexer;
import org.dspace.storage.rdbms.DatabaseManager;
import org.dspace.storage.rdbms.TableRow;

public class UpdateURIPrefix {

	public static void main(String[] args) throws Exception
    {
        // There should be two paramters
        if (args.length < 2)
        {
            System.out.println("\nUsage: update-uri-prefix <old uri> <new uri> (NO traling slash)\n");
        }
        else
        {
            // Confirm with the user that this is what they want to do
            String oldH = args[0];
			String newH = args[1];

            BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
            Context context = new Context();
            System.out.println("If you continue, all URI prefix of the items in your repository with prefix " +
                                oldH + "will be updated to have URI prefix " + newH + ". Also please check that there's NO trailing slash.\n");
            String sql = "SELECT count(*) as count FROM metadatavalue, handle " +
                         "WHERE handle.resource_id = metadatavalue.item_id " +
                         "AND metadata_field_id = 25 AND handle.resource_type_id = 2 AND text_value LIKE '" + oldH + "%';";
            TableRow row = DatabaseManager.querySingle(context, sql, new Object[] {});
            long count = row.getLongColumn("count");
            System.out.println(count + " items will be updated.\n");
            System.out.print("Have you taken a backup, and are you ready to continue? [y/n]: ");
            String choiceString = input.readLine();

            if (choiceString.equalsIgnoreCase("y"))
            {
                // Make the changes
                System.out.print("Updating metadatavalues table... ");
                sql = "UPDATE metadatavalue SET text_value ='" + newH + "/handle/' || handle.handle " +
		          	  "FROM handle WHERE handle.resource_id = metadatavalue.item_id " +
		          	  "AND metadata_field_id = 25 AND handle.resource_type_id = 2 AND text_value LIKE '" + oldH + "%';";
                int updated = DatabaseManager.updateQuery(context, sql, new Object[] {});
                System.out.println(updated + " metadata values updated");

                // Commit the changes
                context.complete();

                System.out.print("Re-creating browse and search indexes... ");                

                // Reinitialise the browse system
                IndexBrowse.main(new String[] {"-i"});

                // Reinitialise the browse system
                try
                {
                    DSIndexer.main(new String[0]);
                }
                catch (Exception e)
                {
                    // Not a lot we can do
                    System.out.println("Error re-indexing:");
                    e.printStackTrace();
                    System.out.println("\nPlease manually run [dspace]/bin/index-all");
                }

                // All done
                System.out.println("\nURI successfully updated.");
            }
            else
            {
                System.out.println("No changes have been made to your data.");
            }
        }
    }
}
