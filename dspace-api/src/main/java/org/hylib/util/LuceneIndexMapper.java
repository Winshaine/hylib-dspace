package org.hylib.util;

import java.util.ArrayList;

import org.dspace.core.ConfigurationManager;

public class LuceneIndexMapper {
	
	/** ArrayList that stores lucene indices */
	private static ArrayList<String> luceneArray;
	
	/** ArrayList that stores search type */
	private static ArrayList<String> searchTypeArray;
	
	public static void fillMappingArray()
	{
		if(luceneArray==null || searchTypeArray==null)
		{
			luceneArray = new ArrayList<String>();
			searchTypeArray = new ArrayList<String>();
		}
		
		int i = 1;
        String sindex = ConfigurationManager.getProperty("search.index." + i);
        while(sindex != null)
        {
            String[] indexString = sindex.split(":");
            luceneArray.add(indexString[1].replace(".*", ""));
            searchTypeArray.add(indexString[0]);
            sindex = ConfigurationManager.getProperty("search.index." + ++i);
        } 
	}
	
	public static ArrayList<String> getLuceneArray()
	{
		return luceneArray;
	}
	
	public static ArrayList<String> getSearchTypeArray()
	{
		return searchTypeArray;
	}
	
	/**
	 * Get lucene index from search type
	 */
	public static String getLuceneFromSearchType(String searchType)
	{
		int index = searchTypeArray.indexOf(searchType);
		
		return luceneArray.get(index);
	}
	
	/**
	 * Get search type from lucene index
	 */
	public static String getSearchTypeFromLucene(String lucene)
	{
		int index = luceneArray.indexOf(lucene);
		
		return searchTypeArray.get(index);
	}
}
