package org.hylib.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil
{
    /** Date format for Dublin core */
    private static final String DC_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    
    /** Another possible format */
    private static final String DC_DATE_FORMAT2 = "yyyy-MM-dd";
    
    /** Date format for display */
    private static final String DISPLAY_DATE_FORMAT = "MMMM dd, yyyy";

    public static Date createDate(String dateString)
    {
        // date format for DC
        SimpleDateFormat format = new SimpleDateFormat(DC_DATE_FORMAT);
        SimpleDateFormat format2 = new SimpleDateFormat(DC_DATE_FORMAT2);

        Date date = null;

        try
        {
            date = format.parse(dateString);
        }
        catch (ParseException e)
        {
            try
            {
                date = format2.parse(dateString);
            }
            catch (ParseException e1)
            {
                e1.printStackTrace();
            }
        }
        
        return date;
    }
    
    public static String getDateString(Date date)
    {
        // date format for DC
        SimpleDateFormat format = new SimpleDateFormat(DISPLAY_DATE_FORMAT);
        
        try{
            return format.format(date);
        }
        catch(Exception e)
        {
            return "";
        }
    }
}
