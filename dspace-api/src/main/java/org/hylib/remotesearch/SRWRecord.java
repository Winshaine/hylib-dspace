package org.hylib.remotesearch;

import java.net.URL;
import java.util.Date;

public class SRWRecord
{
    /** important attributes that we choose to keep */
    private String title;
    private String[] authors;
    private Date issuedDate;
    private String abstractText;
    private URL url;
    
    /**
     * Constructor
     * 
     * @param title - title
     * @param authors - authors
     * @param issuedDate - issuedDate
     * @param abstractText - abstractText
     * @param url - url
     */
    public SRWRecord(String title, String[] authors, Date issuedDate, String abstractText, URL url)
    {
        this.title = title;
        this.authors = authors;
        this.issuedDate = issuedDate;
        this.abstractText = abstractText;
        this.url = url;
    }
    
    /** getters and setters for this class */
    public String getTitle()
    {
        return title;
    }
    
    public void setTitle(String title)
    {
        this.title = title;
    }
    
    public String[] getAuthors()
    {
        return authors;
    }
    
    public void setAuthor(String[] authors)
    {
        this.authors = authors;
    }
    
    public Date getIssuedDate()
    {
        return issuedDate;
    }
    
    public void setIssuedDate(Date issuedDate)
    {
        this.issuedDate = issuedDate;
    }
    
    public String getAbstractText()
    {
        return abstractText;
    }
    
    public void setAbstractText(String abstractText)
    {
        this.abstractText = abstractText;
    }
    
    public URL getUrl()
    {
        return url;
    }
    
    public void setUrl(URL url)
    {
        this.url = url;
    }
    
}
