package org.hylib.remotesearch;

import java.util.ArrayList;

public class SRWResponse
{
    /** entire object of the server */
    private RemoteSearchServer server;
    
    /** actual number of records */
    private int recordCount;
    
    /** a list of resulting records */
    private ArrayList<SRWRecord> records;
    
    /** boolean variable indicating if the server is functioning */
    private boolean functioning;
    
    /**
     * Constructor
     * 
     * @param server - server
     */
    public SRWResponse(RemoteSearchServer server)
    {
        this.server = server;
        records = new ArrayList<SRWRecord>();
    }
    
    /**
     * Add record into the array
     */
    public void addRecord(SRWRecord record)
    {
        records.add(record);
    }
    
    /**
     * Get records
     */
    public ArrayList<SRWRecord> getRecords()
    {
        return records;
    }
    
    /**
     * Set number of records
     */
    public void setRecordCount(int recordCount)
    {
        this.recordCount = recordCount;
    }
    
    /**
     * Get number of records
     */
    public int getRecordCount()
    {
        return recordCount;
    }
    
    /**
     * Get server
     */
    public RemoteSearchServer getServer()
    {
        return server;
    }

    /**
     * Check if the server is functioning
     */
    public boolean isFunctioning()
    {
        return functioning;
    }

    /**
     * Check functioning status of the server
     */
    public void setFunctioning(boolean functioning)
    {
        this.functioning = functioning;
    }
}
