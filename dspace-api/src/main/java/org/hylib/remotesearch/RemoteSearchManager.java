package org.hylib.remotesearch;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.dspace.core.ConfigurationManager;
import org.hylib.util.LuceneIndexMapper;
import org.jdom.Attribute;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import org.apache.log4j.Logger;

public class RemoteSearchManager
{
    /** log4j category */
    private static Logger log = Logger.getLogger(RemoteSearchManager.class);

    /** Remote search server configuration file */
    private static final String RM_SEARCH_CONFIG = "remotesearch.xconf";

    /** List of elements and attributes */
    private final static String SERVERS_ELEMENT = "RemoteServers";
    private final static String SERVER_ELEMENT = "Server";
    private final static String SERVER_NAME_ATT = "name";
    private final static String SERVER_DESC_ATT = "description";
    private final static String SERVER_URL_ATT = "url";
    private final static String SERVER_TIMEOUT_ATT = "connectionTimeout";
    
    private final static String CONNECTION_ELEMENT = "Connection";
    private final static String DEFTIMEOUT_ATT = "defaultTimeout";
    
    private final static String DISPLAY_ELEMENT = "Display";
    private final static String RPP_ATT = "resultPerPage";

    /** Remote search server list */
    private static ArrayList<RemoteSearchServer> serverlist;
    
    /** Default timeout */
    private static int defaultTimeout;
    
    /** Results to be displayed per page */
    private static int resultPerPage;
    
    /**
     * Default constructor for this class' instance
     */
    public RemoteSearchManager()
    {
        serverlist = new ArrayList<RemoteSearchServer>();
    }
    
    // Load configuration from the remote search server configuration file
    public static void loadRMServerConfig() 
    {
        if(serverlist==null){
            serverlist = new ArrayList<RemoteSearchServer>();
        }
        
        String dspace_dir = ConfigurationManager.getProperty("dspace.dir");
        SAXBuilder builder = new SAXBuilder();
        Document doc;
    
        try
        {
            doc = builder.build(new File(dspace_dir + "/config/"
                    + RM_SEARCH_CONFIG));
            
            //start parsing the document
            Element root = doc.getRootElement();
            
            //create server list
            List<Element> servers = root.getChild(SERVERS_ELEMENT).getChildren(SERVER_ELEMENT);
            
            for(int i=0; i<servers.size(); i++)
            {
                Element server = servers.get(i);
                
                //check if this server name already exists
                String serverName = server.getAttributeValue(SERVER_NAME_ATT);
                if(getServerFromName(serverName)!=null) {
                    log.fatal("Duplicate remote server name ("+serverName+")");
                    throw new IllegalArgumentException("Duplicate remote server name ("+serverName+")");
                }
                
                int serverTimeout = -1;
                Attribute timeoutAtt = server.getAttribute(SERVER_TIMEOUT_ATT);
                if(timeoutAtt!=null)
                    serverTimeout = timeoutAtt.getIntValue();
                
                serverlist.add(new RemoteSearchServer(server.getAttribute(SERVER_NAME_ATT).getValue(), 
                        server.getAttribute(SERVER_DESC_ATT).getValue(), 
                        server.getAttribute(SERVER_URL_ATT).getValue(),
                        serverTimeout));
            }
            
            //get default timeout
            defaultTimeout = root.getChild(CONNECTION_ELEMENT).getAttribute(DEFTIMEOUT_ATT).getIntValue();
            
            //get result per page
            resultPerPage = root.getChild(DISPLAY_ELEMENT).getAttribute(RPP_ATT).getIntValue();
        }
        catch (JDOMException e)
        {
            log.fatal("Error parsing remote server config ("+RM_SEARCH_CONFIG+")", e);
            throw new RuntimeException("Error parsing remote server config ("+RM_SEARCH_CONFIG+")", e);
        }
        catch (IOException e)
        {
            log.fatal("Error parsing remote server config ("+RM_SEARCH_CONFIG+")", e);
            throw new RuntimeException("Error parsing remote server config ("+RM_SEARCH_CONFIG+")", e);
        }
        
        //Also loads lucene index for later use in search operations
        LuceneIndexMapper.fillMappingArray();
    }

    //Getter method for remote server list
    public static ArrayList<RemoteSearchServer> getServerlist()
    {
        return serverlist;
    }
    
    //Retrieve the correct server object from the given server id
    public static RemoteSearchServer getServerFromName(String name)
    {
        for(int i=0; i<serverlist.size(); i++)
        {
            RemoteSearchServer serv = serverlist.get(i);
            if(name.equals(serv.getServerName()))
                return serv;
        }
        
        return null;
    }
    
    public static int getDefaultTimeout()
    {
        return defaultTimeout;
    }
    
    public static int getResultPerPage()
    {
        return resultPerPage;
    }
}
