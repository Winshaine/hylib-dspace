package org.hylib.remotesearch;

public class SRWConfig
{
    /** Schema that we use */
    public static String SCHEMA = "info:srw/schema/1/dc-v1.1";
    
    /** Namespace prefix */
    public static String NAMESPACE_PREFIX = "dc";
    
    /** DC index that we would like to get */
    public static String DC_TITLE = "title";
    
    public static String DC_AUTHOR = "contributor.author";
    
    public static String DC_DATE = "date.issued";
    
    public static String DC_ABSTRACT = "description.abstract";
    
    public static String DC_URL = "identifier.uri";
}
