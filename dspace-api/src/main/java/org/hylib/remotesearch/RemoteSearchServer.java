package org.hylib.remotesearch;

public class RemoteSearchServer
{   
    /** Name of the server */
    private String serverName;
    
    /** Description of the server */
    private String serverDescription;
    
    /** URL of the server */
    private String serverUrl;
    
    /** Timeout for the server */
    private int serverTimeout;
    
    /**
     * Constructor for this class' instance
     * 
     * @param name - serverName
     * @param description - serverDescription
     * @param url - serverUrl
     * @param timeout - serverTimeout
     */
    public RemoteSearchServer(String name, String description, String url, int timeout)
    {
        serverName = name;
        serverDescription = description;
        serverUrl = url;
        serverTimeout = timeout;
    }

    /** Below are getter and setter methods **/      
    public String getServerName()
    {
        return serverName;
    }

    public void setServerName(String serverName)
    {
        this.serverName = serverName;
    }

    public String getServerDescription()
    {
        return serverDescription;
    }

    public void setServerDescription(String serverDescription)
    {
        this.serverDescription = serverDescription;
    }

    public String getServerUrl()
    {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl)
    {
        this.serverUrl = serverUrl;
    }

    public int getServerTimeout()
    {
        return serverTimeout;
    }

    public void setServerTimeout(int serverTimeout)
    {
        this.serverTimeout = serverTimeout;
    }
    
    
}
