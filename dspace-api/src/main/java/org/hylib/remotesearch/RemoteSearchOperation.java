/*
 * RemoteSearchOperation.java
 * 
 * Receive a list of servers to send the search request to, 
 * send the requests to those servers, and retrieve the responses 
 * in xml format.
 * 
 * The xml files will be parsed to get the number of results
 * and the identifier (uri) of the result along with some other
 * useful metadata. All metadata will be stored in a SRWRecord
 * for easy retrieval.
 * 
 */
package org.hylib.remotesearch;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.hylib.util.DateUtil;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;
import org.jdom.xpath.XPath;

public class RemoteSearchOperation
{
    /** SRW parameter string ($<param name>$ needs to be replaced later) */
    private static final String SRW_PARAM_STRING = "?query=$query$&version=1.1&operation=searchRetrieve" +
            "&recordSchema=$schema$&startRecord=$startRec$&maximumRecords=$max$&resultSetTTL=300&recordPacking=xml";
    
    /** XPath to all actual records */
    private static final String XPATH_RECORD = "//*[local-name()='records']/*[local-name()='record']/*[local-name()='recordData']/*";
    
    /** XPath for testing if the returned schema is the same as the one we support */
    private static final String XPATH_SCHEMA = "//*[local-name()='records']/*[local-name()='record']/*[local-name()='recordSchema']";
    
    /** XPath for getting actual number of results */
    private static final String XPATH_NUMRESULT = "//*[local-name()='numberOfRecords']";
    
    /**
     * Send request to servers and get responses in xml format
     * 
     * @param query - cql string
     * @param servers - list of remote servers to search
     */
    public static ArrayList<SRWResponse> performRemoteSearch(String query, ArrayList<String> servers, int startRecord, int maxRecord)
    {  
        ArrayList<SRWResponse> responses = new ArrayList<SRWResponse>();
        
        // add parameters to the end part of the query
        String queryEnd = genEndQueryString(query, startRecord, maxRecord);
        
        // get response from all servers
        for(int i=0; i<servers.size(); i++)
        {
            String servname = servers.get(i);
            
            // get entire server object from the server id
            RemoteSearchServer server = RemoteSearchManager.getServerFromName(servname);
            
            // create RMSServerResponse
            SRWResponse servResponse = new SRWResponse(server);            

            Document responseDoc;
            try
            {
                String fullURL = server.getServerUrl() + queryEnd;  
                
                responseDoc = buildXMLDocument(fullURL, server.getServerTimeout());
                if(responseDoc==null)
                { 
                    servResponse.setFunctioning(false);
                    responses.add(servResponse);
                    continue;
                }
                else
                { 
                    servResponse.setFunctioning(true);
                }
                
                
                if(!checkSchema(responseDoc))
                { 
                    responses.add(servResponse);
                    continue;
                }
                
                int recordCount = getTotalRecordCount(responseDoc); 
                servResponse.setRecordCount(recordCount);
                
                // obtain all records
                createSRWRecords(responseDoc, servResponse);
            }
            catch (MalformedURLException e)
            {
                e.printStackTrace();
            }
            catch (JDOMException e)
            {
                e.printStackTrace();
            }
            
            // put this server response into a response list
            responses.add(servResponse);
        }
        
        return responses;
    }
    
    /**
     * Check if the schema is the one supported
     * @throws JDOMException 
     */
    private static boolean checkSchema(Document doc) throws JDOMException
    {
        XPath xp = XPath.newInstance(XPATH_SCHEMA);
        Element schema= (Element)xp.selectSingleNode(doc);
        if(schema==null || !schema.getTextTrim().equals(SRWConfig.SCHEMA))
        {
            return false;
        }
        
        return true;
    }
    
    /**
     * Get the actual total number of records from the query
     * @throws JDOMException 
     */
    private static int getTotalRecordCount(Document doc) throws JDOMException
    {
        XPath xp2 = XPath.newInstance(XPATH_NUMRESULT);
        Element num = (Element)xp2.selectSingleNode(doc);
        
        return Integer.parseInt(num.getText());
    }
    
    /**
     * Parse the response XML document and create SRW record containing all metadata
     * @throws JDOMException 
     * @throws MalformedURLException 
     */
    private static void createSRWRecords(Document doc, SRWResponse response) throws JDOMException, MalformedURLException
    {
        XPath xpath = XPath.newInstance(XPATH_RECORD);
        List list = xpath.selectNodes(doc);
        
        for(int j=0; j<list.size(); j++)
        {
            // get a single record
            Element recordData = (Element)list.get(j);
            
            Namespace ns = recordData.getNamespace(SRWConfig.NAMESPACE_PREFIX);
            
            // get metadata
            String title = (recordData.getChild(SRWConfig.DC_TITLE, ns))==null?"":recordData.getChild(SRWConfig.DC_TITLE, ns).getTextTrim();
            
            List authorList = recordData.getChildren(SRWConfig.DC_AUTHOR, ns);
            String[] authors = new String[authorList.size()];
            for(int k=0; k<authorList.size(); k++)
            {
                authors[k] = ((Element)authorList.get(k)).getTextTrim();
            }
            
            String issuedDate = (recordData.getChild(SRWConfig.DC_DATE, ns))==null?"":recordData.getChild(SRWConfig.DC_DATE, ns).getTextTrim();
            String abstractText = (recordData.getChild(SRWConfig.DC_ABSTRACT, ns))==null?"":recordData.getChild(SRWConfig.DC_ABSTRACT, ns).getTextTrim();
            String url = (recordData.getChild(SRWConfig.DC_URL, ns))==null?"":recordData.getChild(SRWConfig.DC_URL, ns).getTextTrim();
           
            // create RMSRecord object and add to server response object
            response.addRecord(new SRWRecord(title, authors, DateUtil.createDate(issuedDate), abstractText, new URL(url)));
        }
    }
    
    /**
     * Generate an SRW query string containing all parameters 
     * 
     * @param query - search query
     * @param startRec - starting record
     * @param maxRec - maximum number of record
     * @return generated SRW query string
     */
    private static String genEndQueryString(String query, int startRec, int maxRec)
    {
        String endQuery = SRW_PARAM_STRING;
        
        // replace query
        try
        {
            endQuery = endQuery.replace("$query$", URLEncoder.encode(query, "UTF-8"));
        }
        catch (UnsupportedEncodingException e)
        {
            endQuery = endQuery.replace("$query$", query); //cannot happen but just in case
            e.printStackTrace();
        }
        
        // replace schema
        endQuery = endQuery.replace("$schema$", SRWConfig.SCHEMA);
        
        // replace start record
        endQuery = endQuery.replace("$startRec$", ""+startRec);
        
        // replace maximum record
        endQuery = endQuery.replace("$max$", ""+maxRec);
        
        return endQuery;
    }
    
    /**
     * Check the connection to the url and build return xml document of that url
     * if the connection is ok
     * @throws JDOMException 
     */
    private static Document buildXMLDocument(String checkUrl, int timeout) throws JDOMException
    {
        try
        {
            URL url = new URL(checkUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            
            if(timeout!=-1)
                conn.setConnectTimeout(timeout);
            else
                conn.setConnectTimeout(RemoteSearchManager.getDefaultTimeout());

            SAXBuilder builder = new SAXBuilder();
            return builder.build(conn.getInputStream());
        }
        catch (IOException e) 
        {
            e.printStackTrace();
        }
        
        return null;
    }
}
